create table configs
(
    name varchar(30) not null,
    metadata jsonb not null
);

create unique index configs_name_u_index
    on configs (name);

alter table configs
    add constraint configs_pk
        primary key (name);