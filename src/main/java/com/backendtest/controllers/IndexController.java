package com.backendtest.controllers;

import com.backendtest.db.entities.Configs;
import com.backendtest.db.entities.models.Metadata;
import com.backendtest.services.interfaces.ConfigService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

@RestController
public class IndexController {

    private final ConfigService configService;

    public IndexController(ConfigService configService) {
        this.configService = configService;
    }

    // Get all configs
    @GetMapping(value = "/configs")
    public ResponseEntity<List<Configs>> getConfigs() {

        return this.configService.getConfigs();
    }

    // Add new config
    @PostMapping(value = "/configs")
    public ResponseEntity<Map<String, Object>> createConfig(@Valid @RequestBody Configs config){

        return this.configService.addConfig(config);
    }

    // Get config by name
    @GetMapping(value = "/configs/{name}")
    public ResponseEntity<Configs> getConfig(@PathVariable("name") String name) {

        return this.configService.getConfig(name);
    }

    // Update a given config by name
    @PutMapping(value = "/configs/{name}")
    public ResponseEntity<?> updateConfig(@PathVariable("name") String name,
                                          @Valid @RequestBody Metadata metadata) {

        return this.configService.updateConfig(name, metadata);
    }

    // Delete a given config by name
    @DeleteMapping(value = "/configs/{name}")
    public ResponseEntity<?> updateConfig(@PathVariable("name") String name) {

        return this.configService.deleteConfig(name);
    }

    // Get configs based on passed parameters
    @GetMapping(value = "/search")
    public ResponseEntity<?> home(@Valid RequestData metadata) {

        return this.configService.search(metadata);
    }
}
