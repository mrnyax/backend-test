package com.backendtest.controllers;

import com.backendtest.db.entities.models.Metadata;

import javax.validation.constraints.NotNull;

public class RequestData {

    @NotNull
    private Metadata metadata;

    public RequestData() {
    }

    public Metadata getMetadata() {
        return metadata;
    }

    public void setMetadata(Metadata metadata) {
        this.metadata = metadata;
    }
}
