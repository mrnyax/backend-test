package com.backendtest.services.implementations;

import com.backendtest.controllers.RequestData;
import com.backendtest.db.entities.Configs;
import com.backendtest.db.entities.models.Metadata;
import com.backendtest.db.repos.ConfigsRepository;
import com.backendtest.services.interfaces.ConfigService;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class ConfigServiceImpl implements ConfigService {

    private final ConfigsRepository configsRepository;

    public ConfigServiceImpl(ConfigsRepository configsRepository) {
        this.configsRepository = configsRepository;
    }

    @Override
    public ResponseEntity<List<Configs>> getConfigs() {

        return new ResponseEntity<>(this.configsRepository.getAllConfigs(), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Map<String, Object>> addConfig(@Valid Configs config) {

        Map<String, Object> map = new HashMap<>();

        // Check if config name exists
        if(this.configsRepository.findByName(config.getName()).isPresent()) {

            map.put("message", "Config name exists");
            return new ResponseEntity<>(map, HttpStatus.BAD_REQUEST);
        }

        // Save new config
        this.configsRepository.save(config);

        map.put("message", "Config added successfully.");
        return new ResponseEntity<>(map, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Configs> getConfig(String name) {

        // return config if found else return status 404
        return this.configsRepository.findByName(name)
                .map(configs -> new ResponseEntity<>(configs, HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @Override
    public ResponseEntity<?> deleteConfig(String name) {

        Map<String, Object> map = new HashMap<>();

        // Check if config name exists
        if(this.configsRepository.findByName(name).isEmpty()) {

            // Message to advice not found
            map.put("message", "Record not found");
            return new ResponseEntity<>(map, HttpStatus.NOT_FOUND);
        }

        // Delete if exists
        this.configsRepository.deleteById(name);

        // Message to advice record deleted
        map.put("message", "Record deleted.");
        return new ResponseEntity<>(map, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<?> updateConfig(String name, Metadata metadata) {

        Optional<Configs> record = this.configsRepository.findByName(name);

        Map<String, Object> map = new HashMap<>();

        // Check if config name exists
        if(record.isEmpty()) {

            map.put("message", "Record not found");
            return new ResponseEntity<>(map, HttpStatus.NOT_FOUND);
        }

        // Get record and update
        Configs config = record.get();
        config.setMetadata(metadata);

        // Update record
        this.configsRepository.save(config);

        // Message to advice record updated
        map.put("message", "Record updated.");
        return new ResponseEntity<>(map, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<?> search(RequestData metadata) {

        try {
            // Convert to String to use in SQL query with containment operator
            String jsonQuery = new ObjectMapper()
                    .setSerializationInclusion(JsonInclude.Include.NON_NULL) //Remove null fields
                    .writeValueAsString(metadata.getMetadata());

            return new ResponseEntity<>(this.configsRepository.searchByParams(jsonQuery), HttpStatus.OK);

        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
}
