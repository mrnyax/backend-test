package com.backendtest.services.interfaces;

import com.backendtest.controllers.RequestData;
import com.backendtest.db.entities.Configs;
import com.backendtest.db.entities.models.Metadata;
import org.springframework.http.ResponseEntity;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

public interface ConfigService {

    ResponseEntity<List<Configs>> getConfigs();

    ResponseEntity<Map<String, Object>> addConfig(@Valid Configs requestData);

    ResponseEntity<Configs> getConfig(String name);

    ResponseEntity<?> deleteConfig(String name);

    ResponseEntity<?> updateConfig(String name, Metadata metadata);

    ResponseEntity<?> search(RequestData metadata);
}
