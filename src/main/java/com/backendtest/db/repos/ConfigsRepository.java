package com.backendtest.db.repos;

import com.backendtest.db.entities.Configs;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ConfigsRepository extends CrudRepository<Configs, String> {

    @Query(nativeQuery = true, value = "SELECT * FROM configs")
    List<Configs> getAllConfigs();

    @Query(nativeQuery = true, value = "SELECT * FROM configs c WHERE c.name = :name")
    Optional<Configs> findByName(@Param("name") String name);

    @Query(nativeQuery = true, value = "SELECT * FROM configs WHERE metadata @> CAST(:query as jsonb)")
    List<Configs> searchByParams(@Param("query") String query);

}
