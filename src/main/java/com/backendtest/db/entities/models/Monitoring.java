package com.backendtest.db.entities.models;

import java.io.Serializable;

public class Monitoring implements Serializable {

    private static final long serialVersionUID = 1L;

    private boolean enabled;

    public Monitoring() {
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}
