package com.backendtest.db.entities.models;

import java.io.Serializable;

public class Metadata implements Serializable {

    private static final long serialVersionUID = 1L;

    private Monitoring monitoring;
    private Limits limits;

    public Metadata() {
    }

    public Monitoring getMonitoring() {
        return monitoring;
    }

    public void setMonitoring(Monitoring monitoring) {
        this.monitoring = monitoring;
    }

    public Limits getLimits() {
        return limits;
    }

    public void setLimits(Limits limits) {
        this.limits = limits;
    }
}

