package com.backendtest.db.entities.models;

import java.io.Serializable;

public class CPU implements Serializable {

    private static final long serialVersionUID = 1L;

    private boolean enabled;
    private String value;

    public CPU() {
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
