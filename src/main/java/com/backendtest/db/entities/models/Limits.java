package com.backendtest.db.entities.models;

import java.io.Serializable;

public class Limits implements Serializable {

    private static final long serialVersionUID = 1L;

    private CPU cpu;

    public Limits() {
    }

    public CPU getCpu() {
        return cpu;
    }

    public void setCpu(CPU cpu) {
        this.cpu = cpu;
    }
}
