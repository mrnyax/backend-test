package com.backendtest.db.entities;

import com.backendtest.db.entities.models.Metadata;
import com.backendtest.db.utils.JsonbType;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity(name = "Configs")
@Table(name = "configs")
@TypeDef(name = "JsonbType", typeClass = JsonbType.class)
public class Configs implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @NotEmpty
    @NotNull
    @Column(name = "name")
    private String name;

    @NotNull
    @Column(name = "metadata", columnDefinition = "jsonb")
    @Type(type = "JsonbType")
    private Metadata metadata;

    public Configs() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Metadata getMetadata() {
        return metadata;
    }

    public void setMetadata(Metadata metadata) {
        this.metadata = metadata;
    }
}
