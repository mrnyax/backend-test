package com.backendtest.controllers;

import com.backendtest.db.entities.models.Metadata;
import com.backendtest.services.interfaces.ConfigService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

// Test web controllers
@WebMvcTest(controllers = IndexController.class)
class IndexControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private ConfigService configService;

    @Test
    @DisplayName(value = "Test for valid request to expect status 200")
    void whenValidUrlAndMethodAndContentTypeAndRequestBody_thenReturns200() throws Exception {

        Metadata metadata = new Metadata();

        mockMvc.perform(put("/configs/datacenter-1")
                .content(objectMapper.writeValueAsString(metadata))
                .contentType("application/json"))
                .andExpect(status().isOk());

    }

    @Test
    @DisplayName(value = "Test for invalid request to expect status 400")
    void whenValidUrlAndNoRequestBody_thenReturns400() throws Exception {

        mockMvc.perform(put("/configs/datacenter-1")
                .contentType("application/json"))
                .andExpect(status().is4xxClientError());

    }

}