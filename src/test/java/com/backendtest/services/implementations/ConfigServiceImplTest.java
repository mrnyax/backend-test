package com.backendtest.services.implementations;

import com.backendtest.db.entities.Configs;
import com.backendtest.db.repos.ConfigsRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;

import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

//Unit testing example
@ExtendWith(MockitoExtension.class)
class ConfigServiceImplTest {

    @Mock
    private ConfigsRepository configsRepository;

    @InjectMocks
    private ConfigServiceImpl configService;

    @Test
    @DisplayName(value = "Response should contain message key")
    void addConfigTest() {

        Configs config = new Configs();
        config.setName("datacenter-1");

        when(configsRepository.save(any(Configs.class))).thenReturn(config);

        ResponseEntity<Map<String, Object>> response = configService.addConfig(config);

        assertThat((String) Objects.requireNonNull(response.getBody()).get("message"))
                .isNotNull()
                .isNotEmpty();

    }

    @Test
    @DisplayName(value = "Test to check config name exists")
    void configNameExists() {

        Configs config = new Configs();
        config.setName("datacenter-1");

        when(configsRepository.findByName("datacenter-1")).thenReturn(Optional.of(config));

        ResponseEntity<Configs> response = configService.getConfig("datacenter-1");

        assertThat(response.getStatusCode().is2xxSuccessful()).isTrue();
    }


}