# Kyosk Backend Test

The project has been implemented using Java Spring Boot and the database used is PostgreSQL.

It is a simple HTTP service that stores and returns configurations that satisfy certain conditions.

The app has been automated using bitbucket pipelines. However, the same application can deployed on a local kubernetes
cluster (minikube). The instructions provided can be used to deploy the app on minikube.

### IP address

- 34.66.184.248 for remote hosted application.

### Automation - (Only for remote deployment)

Bitbucket pipelines has been used to automate the app deployment into kubernetes.

The developer has used kubernetes on GCP rather than minikube due to resource limitation on the developer laptop.

Though not a requirement for the test, Kong API gateway has been set on GKE

The same commands can be used to run the app on a local kubernetes cluster(minikube).

## Prerequisite

The following are required to run the app

- kubectl
- kubernetes (minikube to run locally)
- Docker

In order to deploy the application locally, Minikube is required to provide a single node Kubernetes cluster and YAML
templates to create the Kubernetes objects.

The objective is to deploy a spring boot application that uses PostgreSQL databases for storage.

To run the app locally, a local docker registry running on minikube has been used.

#### The app has the following endpoints:

1. /configs - A GET method that returns all configs
2. /configs - A POST method that creates/add a new config
3. /configs/{name} - A GET method that returns config of the specified name
4. /configs/{name} - A PUT method that updates a config of the specified name
5. /configs/{name} - A DELETE method that deletes a config of the specified name
6. /search?metadata.key=value - Searches based on supplied keys and values

## Running minikube and setting up container registry (Only for local set up)

Use the following command to start minikube specify the virtual box and also allowing insecure container registries.

```
minikube start — vm-driver=virtualbox --insecure-registry "10.0.0.0/24"
```

Start a registry inside a pod minikube

```
minikube addons enable registry
```

Run `kubectl get pods --namespace kube-system` to get registry name to replace `<registry-pod name>` below

Run the following commands to forward all traffic to the registry. Execute both commands on separate terminal windows

```
kubectl port-forward --namespace kube-system <registry-pod name> 5000:5000
docker run --rm -it --network=host alpine ash -c "apk add socat && socat TCP-LISTEN:5000,reuseaddr,fork TCP:host.docker.internal:5000"
```

The registry can now be accessed on `http://localhost:5000/v2/_catalog`

## Deploying the Spring Boot Backend Application

Once the code is committed and pushed, unit tests will run and once passed, it will be containerized and pushed to
google cloud registry and automatically deployed using bitbucket pipelines.

To deploy the app on minikube, run the following command. It will containerize the app and create a docker image and
push it to the local registry running on minikube.

```
./mvnw compile jib:build -Dimage="localhost:5000/backend-test-app" -Djib.allowInsecureRegistries=true
```

## Kubernetes manifest files

The following manifest templates are used to deploy the app on minikube

### Service file to expose the app

The yaml template below uses a Nodeport type of Service to allow for inbound connections on the Minikube node to reach
the web app Pod, so that we can reach the app from a terminal and from a browser on the Host

```
apiVersion: v1
kind: Service
metadata:
  labels:
    app: backend
  name: backend-service
spec:
  type: NodePort
  ports:
    - nodePort: 31600
      port: 8080
      protocol: TCP
      targetPort: 7000
  selector:
    app: backend
```

Verify is service has been appplied by running

```
kubectl get services
```

Expected output

```
NAME              TYPE        CLUSTER-IP     EXTERNAL-IP   PORT(S)          AGE
backend-service   NodePort    10.98.216.23   <none>        8080:31600/TCP   23m
hello-minikube    NodePort    10.111.111.2   <none>        8080:32586/TCP   8d
kubernetes        ClusterIP   10.96.0.1      <none>        443/TCP          8d
```

### Spring Boot App Deployment Template File

The yaml template below describes the deployment of the spring boot app. Environment variables are passed to the
container, so it can connect to the database. Server post is also set as an environment variable.

```
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app: backend
  name: backend
spec:
  replicas: 1
  selector:
    matchLabels:
      app: backend
  template:
    metadata:
      labels:
        app: backend
    spec:
      containers:
        - image: localhost:5000/backend-test-app
          name: backend
          ports:
            - containerPort: 8080
          env:
            - name: SERVE_PORT
              value: "7000"
            - name: DB_USER
              value: user
            - name: DB_PASS
              value: Test@1234
            - name: DB_NAME
              value: config_db
            - name: DB_PORT
              value: "5432"
            - name: DB_HOST
              value: 104.154.223.167
```

Check if the app is successfully deployed by running the following command

```
kubectl get deployments
```

Expected output

```
NAME             READY   UP-TO-DATE   AVAILABLE   AGE
backend          1/1     1            1           21m
hello-minikube   1/1     1            1           8d
```

It may take a while to successfully deploy. Run the following commands to view logs. The first one gives the pod name
that is used in the second command to view the logs

```
kubectl get pods
kubectl logs <pod-name>
```

To access the app outside the minikube cluster, execute the following command

```
kubectl port-forward service/backend-service 8080:8080
```

The app is not accessible locally on http://localhost:8080/

